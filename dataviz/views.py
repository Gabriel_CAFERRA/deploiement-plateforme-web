import csv
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from .models import Letter
from .models import Election
##############
### Letter.objects.all()
##############


# Create your views here.

#contexte={"data":[42,13,56,12],"label":['Data scientist','Data analyst',
                #'Data Engineer','Data manager']}

#@logout
def logout_view(request): 
    logout(request)
    return render(request, 'registration/logged_out.html')

@login_required
def homepage(request):
    return render(request, 'dataviz/home.html')

def dashboard(request):
    return render(request, 'dataviz/home.html')

def importation(request):
    if request.method == "GET":
        return render(request, 'dataviz/import.html')
    file_csv = request.FILES['file']
    data = [row for row in csv.reader(file_csv.read().decode('UTF-8').splitlines())]
    for row in data[1:]:
        Election.objects.create(
                bureau=row[0],
                votants=row[1],
                )
    return render(request, 'dataviz/home.html')


def visualisation(request):
    context = {
        'label' : [elt.bureau for elt in Election.objects.all()],
        'data' : [elt.votants for elt in Election.objects.all()],
    }
    return render(request, 'dataviz/visu.html', context=context)


def exportation_repo_local(request):
    
    with open('export.csv', 'w', newline='') as csvfile:
        fieldnames = ['bureau', 'votants']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for elt in Election.objects.all():
            writer.writerow({'bureau': elt.bureau,'votants':elt.votants})
    
    return render(request, 'dataviz/export.html')
    

"""def exportation(request):
    context = {
        'label' : [elt.bureau for elt in Election.objects.all()],
        'data' : [elt.votants for elt in Election.objects.all()],
    }
    if request.method == "GET":
        return render(request, 'dataviz/export.html')
    with open('export.csv', 'w', newline='') as csvfile:
        fieldnames = ['bureau','votants']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for elt in Election.objects.all():
            writer.writerow({'bureau': elt.bureau,'votants':elt.votants})
    #response = HttpResponse(csvfile, content_type='file')
    #request['Content-Disposition'] = 'attachment; filename="export.csv"'
    return render(context, 'dataviz/export.html')
    #return HttpResponse(csvfile, content_type='file')"""


def exportation(request):

    # https://docs.djangoproject.com/fr/3.1/howto/outputting-csv/

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export.csv"'
    
    fieldnames = ['bureau', 'votants']
    writer = csv.DictWriter(response, fieldnames=fieldnames)
    writer.writeheader()

    for elt in Election.objects.all():
        writer.writerow({'bureau': elt.bureau,'votants':elt.votants})

    return response

