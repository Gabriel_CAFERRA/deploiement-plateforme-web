from django.db import models

# Create your models here.

# pour créer une table, on définit une classe qui aura le même nom
# que ma table dans SQL

# Majuscule pour Model et aussi pour Letter (pep8)

# models.Model esr l'endroit où je peux gérer la données

class Letter(models.Model):
    label = models.CharField(max_length=30)
    data = models.IntegerField()

    # une fonction dans une classe est une méthode
    
    # une chaîne de caractère dans le périmètre de cette classe
    # respecter ce format
    def __str__(self):
        return f"{self.label} - {self.data}"

# on vient de préciser un format pour la classe qui sera 
# appliquer ensuite par Django, du moment que la classe 
# est Letter 

class Election(models.Model):
    bureau = models.CharField(max_length=200)
    votants = models.IntegerField()

    def __str__(self):
        return f"Données du bureau {self.bureau} - Votants {str(self.votants)}"

class Maps(models.Model):
    nom = models.CharField(max_length=100)
    longitude = models.FloatField()
    lattitude = models.FloatField()

    def __str__(self):
        return f"Nom {self.nom} - Longitude {str(self.longitude)} - Lattitude {str(self.lattitude)}"