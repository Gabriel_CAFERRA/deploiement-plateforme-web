from django.urls import path
from . import views

urlpatterns = [
    #path('import-des-données/', views.importation, name='importation'),
    path('', views.homepage, name='homepage'),
    path('import-des-données/', views.importation, name='importation'),
    path('visualisation-des-données/', views.visualisation, name='visualisation'),
    path('export-des-données/',views.exportation, name='exportation'),
    path('déconnexion/',views.logout_view, name='deconnexion'),

]
